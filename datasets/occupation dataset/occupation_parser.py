
import pandas as pd
templates_path = './all_sentences.tsv'

def generate_dataset(gendered_output_classes=True, non_gendered_output_type=bool):
    templates = pd.read_csv(templates_path, sep='\t')
    templates['bias'] = templates['sentid'].str.extract(r'(\w*.\w*$)')
    templates['bias'].replace('.txt', '', regex=True, inplace=True)
    df = templates[['bias', 'sentence']]
    
    if not gendered_output_classes:
        df.loc[df['bias'] == 'neutral', 'bool_bias'] = False
        df.loc[df['bias'] != 'neutral', 'bool_bias'] = True
        df['bias'] = df['bool_bias']
        df['bias'] = df['bias'].astype(non_gendered_output_type)
        df = df[['bias', 'sentence']]        
    
    return df

if __name__ == '__main__':
    df = generate_dataset(gendered_output_classes=True, non_gendered_output_type=int)
    df = df.rename(columns={'sentence': 'text'})
    
    df['bias'] = df['bias'].apply(lambda x: x[0]) 
    print(df)
    df.to_csv('./dataset.csv', sep=';', index=False)