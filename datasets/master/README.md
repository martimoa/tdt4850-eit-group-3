# /datasets/master
This folder should contain one, and only one, dataset.
The dataset should be a combination of all the data we currently have collected in the form
"some sentence";"bias_marker"

Where the "bias_marker" will be replaced with the sentence label ("m" for male bias, "f" for female bias or "n" for neutral)

The directory /datasets/master/subsets should contain each of the individual subsets collected from different sources, structured in the same manner as mentioned above. 