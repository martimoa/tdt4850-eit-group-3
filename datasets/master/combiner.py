import pandas as pd


paths = [
    '../textmining/dataset.csv',
    '../occupation dataset/dataset.csv'
]

def combine(df1, df2, common_cols=['text', 'bias']):
    df1_subset = df1[common_cols]
    df2_subset = df2[common_cols]

    df = pd.concat([df1_subset, df2_subset], ignore_index=True)
    return df

def load_data():
    cols = ['text', 'bias']
    df = pd.DataFrame(columns=cols)
    for path in paths:
        print(path)
        dataset = pd.read_csv(path, sep=';')
        df = combine(df, dataset, common_cols=cols)

    return df
    

if __name__ == '__main__':
    df = load_data()
    df = df.dropna()
    df.to_csv('./dataset.csv', sep=';', index=False)