import re
import requests
from bs4 import BeautifulSoup as bs
import pandas as pd 

def scrape(website_url):
    res = requests.get(website_url)
    res = bs(res.text, 'html.parser')

    webpage = res.getText()

    oc = re.finditer(r"([a-zA-Z\s']*:)(?<=:)([^:]+)^", webpage, re.MULTILINE)

    output = "CHARACTER;SCRIPT_LINE\n" # .csv file headings
    for idx, match in enumerate(oc, start=1):    
        groups = match.groups()
        actor = re.sub(r"^\s|:", '', groups[0])
        actor = actor.strip()
        actor = re.sub(r"\"+", '', actor)
        actor = re.sub(r"\'+", '', actor)


        lines = groups[1]
        # Replace any newline symbols (\n) with a space ' '
        lines = re.sub(r"\n+", ' ', lines)
        # Replace 1 or more whitespace character with a single whitespace
        lines = re.sub(r"\s{2,}", ' ', lines)

        # Remove anything inside parenthesis, brackets or curly brackets (all including the brackets themselves)
        lines = re.sub(r"\(([^\)]+)\)", '', lines) # remove parenthesis and contents
        lines = re.sub(r"\[([^\]]+)\]", '', lines) # remove brackets and contents
        lines = re.sub(r"\{([^\}]+)\}", '', lines) # remove curly brackets and contents
        
        lines = lines.strip() # Remove leading and trailing whitespace characters
        # Remove any ' and " in the text
        lines = re.sub(r"\"+", '', lines)
        lines = re.sub(r"\'+", '', lines)

        if actor != "" and lines != "" and len(actor) <= 12: # If character name is too long, probably error with regex and ignore line
            output += "\"" + actor + "\";\"" + lines + "\"\n"

    return output    
    
if __name__ == '__main__':
    disney_scripts = [
        ('http://www.fpx.de/fp/Disney/Scripts/SleepingBeauty/sb.html', 'sleeping_beauty'),
        ('http://www.fpx.de/fp/Disney/Scripts/TRDU.html', 'the_rescuers_down_under'),
        ('http://www.fpx.de/fp/Disney/Scripts/LittleMermaid.html', 'the_little_mermaid'),
        ('http://www.fpx.de/fp/Disney/Scripts/BeautyAndTheBeast.txt', 'beauty_and_the_beast'),
        ('http://www.fpx.de/fp/Disney/Scripts/Aladdin.txt', 'aladdin'),
        ('http://www.lionking.org/scripts/Script.html', 'the_lion_king'),
        ('http://www.fpx.de/fp/Disney/Scripts/AGoofyMovie.html', 'a_goofy_movie'),
        ('http://www.fpx.de/fp/Disney/Scripts/HunchbackOfNotreDame.txt', 'hunchback_of_notre_dame'),
        ('http://www.fpx.de/fp/Disney/Scripts/Mulan.html', 'mulan'),
    ]

    for script in disney_scripts:
        url, script_name = script
        print(script_name, url)
        csv = scrape(url)

        f = open('./scripts/' + script_name + '.csv', 'w')
        f.write(csv)
        f.close()

    df = pd.read_csv('./sleeping_beauty.csv', sep=';')
    print(df.head())
