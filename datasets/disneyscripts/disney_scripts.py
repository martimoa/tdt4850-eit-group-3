from bs4 import BeautifulSoup as bs
import requests
import re
import pandas as pd

sleeping_beauty_url = 'http://www.fpx.de/fp/Disney/Scripts/SleepingBeauty/sb.html'

res = requests.get(sleeping_beauty_url)
res = bs(res.text, 'html.parser')

def deeper(elem, actor=None, text=None):
    if len(elem.findChildren()) == 1:
        actor = elem.text
        m = re.search(r'\D*:', actor)
        if m:
            actor = m.group(0).replace(':', '')

        for child in elem.findChildren():
            actor, text = deeper(child, actor, text)
            return actor, text
    
    elif len(elem.findChildren()) > 0:
        for child in elem.findChildren():
            actor, text = deeper(child, actor, text)
            return actor, text
    else:
        return actor, elem.text.replace('\n', ' ').replace('\t', ' ')


script = res.body.find_all('dl')
actors = []
lines = []

for s in script:
    actor, line = deeper(s)
    if actor is not None:
        actors.append(actor)
        lines.append(line)

df = pd.DataFrame(data={'actors': actors, 'lines': lines})

print(df)

