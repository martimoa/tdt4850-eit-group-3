title: The Rabbit Prince
author: NICOLAS FERRYKATARINA EVSTIGNEEVALESSANDRO CONTIUMBERTO LUNDARI
source: NA

Once upon a time there was a small village, surrounded by an enchanted forest, where a beautiful girl named Nicole lived: a sixteen-year-old girl with brown hair, green eyes like hope and a brightly colored dress ...
One day she decided to take a walk in the woods, but as soon as she entered, instead of trees she saw a beautiful garden full of fragrant flowers. She was attracted by the roses that adorned pretty paths, she began to walk one, then another, at a certain point she realized she was lost in that green labyrinth.
Worried, she looked around and saw a cute little bunny, who strangely started talking and introduced himself: “My sweet girl is Randy. Why do you wander all alone in the enchanted garden? " She replied, “I am lost. How can I get out of the garden and the woods? "
The rabbit became serious and ruled: "Only if you help me find my childhood best friend Viola and defeat Black Spot, her dragon, will you find the way home".
The girl, although full of fear for the rabbit's requests, said with great courage: "I accept your conditions".
Day broke and two set off in search of Black Spot's lair.
The journey was long and tiring as the rabbit kept picking up twigs that Nicole had to carry. At one point a huge lava lake blocked their path.
The rabbit got the sticks and, in the blink of an eye, built a boat that could withstand the heat, with which the two managed to overcome the lava lake very easily.
Arriving on the other bank, they found themselves facing a gigantic mountain, at the top they could see huge walls, inside which was the den of Macchia Nera.
The problem was that the mountain didn't have a path to climb. The rabbit then began to jump up to show Nicole where to climb.
Suddenly they felt the ground vibrate: it was an earthquake, the mountain split in two.
"How are we going to save ourselves?" Nicole yelled from a boulder to the rabbit who had managed to reach a small ledge so as not to fall.
While the girl was desperate and the rabbit tried to console her, a cloud of light enveloped them and a voice said: "I am Viola, the fairy of the enchanted garden, your childhood friend, dear Randy and now also your sweet friend Viola" .
Great was the joy of the rabbit, then the magical fairy gave them a magical medallion, which had the power to turn anything into ice.
At that moment a spark of light appeared that guided them to a path in the rock that led to the top, in the den of Black Spot.
They encountered precipices and chasms that forced them to jump a couple of times, risking falling and dying.
When they reached the top, the three found themselves in front of gigantic walls covered with moss, with two huge doors that mysteriously spoke with an ancient and powerful voice: “If she remarries you will find, the doors will open wide”.
And here, immediately after having pronounced an arcane formula, they articulated the question:
  “After a morning of work in the fields, two fathers and two sons decide to take a break by eating some bread with an egg. In the hen house they find only three eggs. Despite this they manage to eat an egg each, why? "
Friends pondered for a long time, then found the answer:
  “The fathers are the grandfather and the grandfather's son. The children are the son of the grandfather and his son. " Randy replied. “So there are three people,” Nicole added, “the grandfather, the son and the grandson. Three eggs for three people. "
The doors complimented themselves and opened. Friends finally crossed the threshold. Entering inside the huge walls, Black Spot saw them, was furious and threatened them: "Now you will meet my powerful Dragonfire Dragon and with me."
The battle was interminable, Randy was exhausted and was about to succumb, when he remembered the medallion he received from his friend. She took it and pointed it at Bradifuoco and his master: the two froze instantly. Immediately a dazzling light appeared that enveloped Randy in a blue and pink cloud, at that same moment we saw the rabbit's hind legs turn into feet, the front legs instead became human hands and, in no time at all, the whole rabbit he became a beautifully dressed prince.
Finally, thanks to the friendship of Nicole and Viola, Randy had broken the spell.
Then the three friends locked Black Spot and her dragon in a cell, of which the fairy took the key into custody.
The prince took Nicole to his kingdom, where the wedding was celebrated and they lived happily ever after.
