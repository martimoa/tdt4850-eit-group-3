#/datasets/rubegni
This dataset contains stories written by children, originally in italian but translated by Raj of our team into english. 
The stories are gathered as individual textfiles, formatted in the following way.
```
title: The story title
author: The story author
source: source of the story, by url if available

the text itself
```
