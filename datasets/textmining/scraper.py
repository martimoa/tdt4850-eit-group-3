import requests
from bs4 import BeautifulSoup as bs
from bs4 import Tag, NavigableString
import numpy as np
import re
import time
import sys
from os import path
import os
import pathlib
import pprint
PROJECT_PATH = pathlib.Path(__file__).parent.parent.parent.absolute()

base_url = 'https://americanliterature.com'
file_heading = "title: {title}\nauthor: {author}\nsource: {source}\n\n"

def is_full_url(url):
    is_full = re.match(r"http[s]{0,1}://www.americanliterature.com.*", url)
    return bool(is_full)

def is_relative_path(url):
    is_relative = re.match(r"^(\/[^\/\n]+)+", url)
    return bool(is_relative)

def same_origin(url):
    return is_full_url(url) or is_relative_path(url)

def get_links(jumbotron, is_valid=None):
    raw_links = jumbotron.find_all('a', href=True)
    links = [link['href'] for link in raw_links if same_origin(link['href'])]
    # if visited is not None and type(visited) == list:
    #     links = [link['href'] for link in raw_links if same_origin(link['href']) and link not in visited]
    
    if is_valid is not None:
        output = [link for link in links if is_valid(link)]
        links = output

    return links

def get_main_content(soup):
    return soup.find(class_='jumbotron')

def is_author_link(url):
    has_author = re.match(r"\/author\/", url)
    return bool(has_author)

def is_shortstory_link(url):
    has_shortstory = re.match(r"\/short-story\/", url)
    return bool(has_shortstory)

def is_childrens_link(url):
    has_childstory = re.match(r"\/childrens-stories\/", url)
    return bool(has_childstory)

def get_title(jumbotron):
    h1 = jumbotron.find('h1', itemprop='name')
    if h1 is not None:
        return h1.getText()
    return None

def to_relative_path(full_url):
    url = re.sub(r"http[s]*://www.americanliterature.com", "", full_url)
    return url

def save(story_obj, link):
    if story_obj is None:
        # print('Story is missing: ', link)
        return False

    for key in story_obj.keys():
        if story_obj[key] is None:
            print('Story is missing {key}: {link}'.format(key=key, link=link))
            return False

    title, author, story = story_obj['title'], story_obj['author'], story_obj['text']
    if  title.strip() == '' or author.strip() == '' or story.strip() == '' or link.strip() == '':
        print('Story is missing data: \n\ttitle: "{t}"\n\tauthor: "{a}"\n\tlink: "{l}"\n\tstory: "{s}"'.format(t=title, a=author, l=link, s=story))
        return False
        
    filename = re.sub(r"\s+", '_', title.lower()) + '-' + re.sub(r"\s+", '_', author.lower()) + '.txt'
    filepath = str(pathlib.Path(__file__).parent.absolute())
    filepath = filepath + '/texts/' + filename
    
    # if path.exists(filepath):
    f = open(filepath, 'w')
    f.write(file_heading.format(title=title, author=author, source=(base_url + link)))
    f.write(story)
    f.close()
    return True

def parse_page(soup, link):
    # return a list of new links and optional story content
    jumbotron = get_main_content(soup)
    a_tags = soup.find_all('a', href=True)
    new_links = [a['href'] for a in a_tags]
    if jumbotron is None:
        return new_links, None, None

    if 'itemtype' not in jumbotron.attrs.keys():
        no_links_here, story = parse_ambigous(soup, link)
        return new_links, story, None
    
    if jumbotron['itemtype'] == 'https://schema.org/Person':
        return new_links, None, jumbotron['itemtype']

    elif jumbotron['itemtype'] == 'https://schema.org/ShortStory':
        title = get_title(jumbotron)
        if title is None or title.strip() == '':
            title = jumbotron.find('h2', itemprop='name')
            title = title.getText() if title is not None else 'cannot find title'

        author = jumbotron.find('small', itemprop='author')
        if author is not None:
            author = author.find('a')

        author = author.getText() if author is not None else None
        # could not find the author name on page, try to see if it is present in link
        if author is None:
            m = re.match(r"\/author\/([^\/]+)", link)
            if m is not None:
                author = m.group(1).strip()
                author = re.sub(r"-", ' ', author).title()
        
        if author is None:
            author = 'unknown'

        start = jumbotron.find('hr')
        
        while start is not None and start.name != 'p':
            start = start.next_sibling

        story = ''
        while start is not None and start.name == 'p':
            story += (start.getText().strip() + '\n\n')
            start = start.next_sibling
        
        if story is not None and story.strip() == '':
            pres = jumbotron.find_all('pre')
            story = ''
            for pre in pres:
                story += (pre.getText().strip() + '\n\n')

        return new_links, {'title': title, 'author': author, 'text': story}, jumbotron['itemtype']
        
    elif jumbotron['itemtype'] == 'https://schema.org/Book':
        nothing_here, story = common(soup, link)
        return new_links, story, jumbotron['itemtype']

    elif jumbotron['itemtype'] == 'https://schema.org/FairyTale':
        title = get_title(jumbotron)
        author = jumbotron.find('a', itemprop='author')
        if author is not None:
            author = author.getText()
        else:
            author = 'unknown'
    
        smalls = jumbotron.find_all('small')
        
        if len(smalls) >= 2:
            smalls = smalls[1]
        else:
            return new_links, None, None

        sibling = smalls.find('hr')
        while sibling is not None and sibling.name != 'p':
            sibling = sibling.next_sibling
        
        text = ''
        while sibling is not None and sibling.name == 'p':
            text += sibling.getText().strip() + '\n\n'
            sibling = sibling.next_sibling
        
        return new_links, {'title': title, 'author': author, 'text': text}, jumbotron['itemtype']


    print('No parse found for {l} with itemtype {i}'.format(l=link, i=jumbotron['itemtype']))
    return new_links, None, None

def common(soup, link):
    # parse all p tags that occur after the first hr tag inside the jumbotron, and to the last p tag, as the story content
    jumbotron = get_main_content(soup)

    title = get_title(jumbotron)
    author = jumbotron.find('a', itemprop='author')
    if author is not None:
        author = author.getText()
    else: 
        author = 'unknown'

    sibling = jumbotron.find('hr')
    while sibling is not None and sibling.name != 'p':
        sibling = sibling.next_sibling
    
    text = ''
    while sibling is not None and sibling.name in ['p', 'pre']:
        text += sibling.getText().strip() + '\n\n'
        sibling = sibling.next_sibling
    
    return [], {'title': title, 'author': author, 'text': text}

def parse_ambigous(soup, link):    
    if is_childrens_link(link):
        return common(soup, link)
    
    m = re.match(r"\/author\/([^\/]+)\/book\/([^\/]+)\/([^\/]+)", link)
    if m is not None:
        author = m.group(1).strip()
        author = re.sub(r"-", " ", author).title()
        title = m.group(2).strip()
        title = re.sub(r"-", " ", title).title()
        chapter = m.group(3).strip()
        chapter = re.sub(r"-", " ", chapter).lower()
        title = title + ' ' + chapter

        jumbotron = soup.find('div', class_='jumbotron')
        hrs = jumbotron.find_all('hr')

        if len(hrs) >= 2:
            sib = hrs[1]


        while sib is not None and sib.name != 'p':
            sib = sib.next_sibling
        
        text = ''
        while sib is not None and sib.name in ['p', 'pre']:
            text += sib.getText().strip() + '\n\n'
            sib = sib.next_sibling

        return [], {'title': title, 'author': author, 'text': text}
    
    return [], None
    
def num_saved():
    filepath = os.path.join(str(pathlib.Path(__file__).parent.absolute()), './texts/')
    paths = pathlib.Path(filepath).iterdir()
    count = 0
    for path in paths:
        if path.is_file():
            count += 1
    return count


def scrape(linkhandler, sleeptime_seconds=1, follow_links=True, verbose=False):
    # res = requests.get('https://americanliterature.com/short-stories-for-children')
    # soup = bs(res.content, 'html.parser')

    # jumbotron = get_main_content(soup)
    # links = get_links(jumbotron)
    # linkhandler.add(links)
    num_fails = 0
    for i,link in enumerate(linkhandler):
        # Wait some seconds, to reduce load on americanliterature.com server(s)
        time.sleep(sleeptime_seconds)

        try:
            # Visit link and parse the HTML with beautifulsoup
            url = base_url + link if is_relative_path(link) else link
            res = requests.get(url)
            soup = bs(res.content, 'html.parser')
            
            link = to_relative_path(link)
            print('Current {i} not visited {t} visited {v} link {l}'.format(i=i, t=len(linkhandler.not_visited), l=link, v=len(linkhandler.visited)))
            
            # Parse the page, will return some (possibly) new links and a story, along with the page itemtype (itemtype attribute of the jumbotron div containing the main content of the page)
            new_links, story, itemtype = parse_page(soup, link)
            new_links = [link for link in new_links if same_origin(link)]

            if follow_links:
                before_add = len(linkhandler)
                linkhandler.add(new_links)
                after_add = len(linkhandler)
                msg = (
                    '{s}num links: {ls}\n'
                    '{s}itemtype {i}\n'
                    '{s}title: {t}\n'
                    '{s}author: {a}\n'
                    '{s}story: {st}\n'
                ).format(
                    s='\t',
                    ls=len(new_links),
                    i=repr(itemtype),
                    t=story['title'] if story is not None and 'title' in story.keys() else 'None',
                    a=story['author'] if story is not None and 'author' in story.keys() else 'None',
                    st=repr(story['text'][:15]) + '...' if story is not None and 'text' in story.keys() else 'None',
                )
                # if verbose:
                #     print(msg)
                
                # if itemtype is not None and itemtype != 'https://schema.org/Person':
                #     print(msg)

            # Save the story. If there was no story found the story will not be saved (e.g. no empty story files)
            saved = save(story, link)
            if i % 10 == 0:
                print('Currently {n} files have been found and saved.'.format(n=num_saved()))
        except Exception as e:
            print('something went wrong:')
            print(e)
            num_fails += 1
            if num_fails > 10:
                raise Exception('The amount of errors if to damn high!')

if __name__ == '__main__':
    from LinkHandler import LinkHandler
    handler = LinkHandler(initial_url='https://americanliterature.com/short-stories-for-children', load_from_file=True, save_on_iter=True, verbose=False)
    print(handler)

    scrape(handler, sleeptime_seconds=0, follow_links=True, verbose=True)
    