import matplotlib.pyplot as plt
import numpy as np
import pathlib
import os
import re
PROJECT_PATH = pathlib.Path(__file__).parent.parent.parent.absolute()

def get_stories():
    path = str(PROJECT_PATH) + '/datasets/textmining/texts/'
    filelist = os.listdir(path)
    stories = []
    for file in filelist:
        if file.endswith('.txt'):
            filepath = path + file
            f = open(filepath, 'r')
            contents = f.read()
            f.close()

            # title_reg = "title:\s*(.*)\n+"
            # author_reg = "author:\s*(.*)\n+"
            # source_reg = "source:\s*(.*)\n+"

            title_m = re.search(r"(?<=title: )([^\n])+", contents)
            author_m = re.search(r"(?<=author: )([^\n])+", contents)
            source_m = re.search(r"(?<=source: )([^\n])+", contents)

            title, author, source = None, None, None
            if title_m is not None:
                title = title_m.group(0)
            if author_m is not None:
                author = author_m.group(0)
            if source_m is not None:
                source = source_m.group(0)


            story = contents[source_m.span(0)[1]:].strip()
            
            data = {
                'title': title,
                'author': author,
                'source': source,
                'text': story
            }
            stories.append(data)
    return stories

def wordcount(story_obj):
    text = story_obj['text']
    text = re.sub(r"\s+", ' ', text)
    text = re.sub(r"\n{2,}", ' ', text)
    words = text.split()
    return len(words)

def charcount(story_obj):
    text = story_obj['text']
    text = re.sub(r"\s+", ' ', text)
    text = re.sub(r"\n{2,}", ' ', text)
    return len(text)

def unique_authors(stories):
    output = {}
    for s in stories:
        if s['author'] not in output.keys():
            output[s['author']] = 1
        else:
            output[s['author']] += 1
    return output

if __name__ == '__main__':
    stories = get_stories()
    print('Total number of stories collected:', len(stories))
    authors = unique_authors(stories)
    import pprint 
    pprint.pprint(authors)
    print(np.sum(list(authors.values())))
    print('Number of unique authors:', len(unique_authors(stories).keys()))
    wordcounts = [wordcount(s) for s in stories]
    charcounts = [charcount(s) for s in stories]

    wc_stats = (
        'Wordcount mean {m:.4f} +/- {std:.4f} min {mn} max {mx}'
    ).format(
        m=np.mean(wordcounts), 
        std=np.std(wordcounts),
        mn=np.min(wordcounts),
        mx=np.max(wordcounts),
    )

    cc_stats = (
        'Charchount mean {m:.4f} +/- {std:.4f} min {mn} max {mx}'
    ).format(
        m=np.mean(charcounts), 
        std=np.std(charcounts),
        mn=np.min(charcounts),
        mx=np.max(charcounts),
    )
    
    print(wc_stats)
    print(cc_stats)

    # plt.hist(wordcounts, bins=60)
    # plt.show()
    # plt.scatter(wordcounts, charcounts)
    # plt.show()