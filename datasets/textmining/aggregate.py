import pandas as pd
import numpy as np

def biased(male_count, female_count, neutral_count):
    if male_count > female_count and male_count > neutral_count:
        return 'm'
    elif female_count > male_count and female_count > neutral_count:
        return 'f'
    elif neutral_count > female_count and neutral_count > male_count:
        return 'n'
    else:
        return 'n'

if __name__ == '__main__':
    df = pd.read_csv('./dataset.csv', sep=';')
    df['bias'] = df.apply(lambda x: biased(x['male_count'], x['female_count'], x['neutral_count']), axis=1)
    df.to_csv('./dataset.csv', sep=';', index=False)