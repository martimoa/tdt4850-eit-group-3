import re
import pathlib
import os

class LinkHandler:
    def __init__(self, initial_url=None, load_from_file=True, visit_on_iter=True, save_on_iter=False, verbose=False, log_prefix='\t\t', log_suffix=''):
        self._verbose_prefix = log_prefix
        self.__verbose_suffix = log_suffix
        self.base_url = initial_url
        self.visited = [] # list of visited links
        self.not_visited = [] # list of known but currently not visited links
        if load_from_file:
            if verbose:
                # print('{prefix}LinkHandler: loading links...'.format(prefix=self._verbose_prefix))
                self.__log('loading links...')
                self.__load__(self.__get_rootpath__())
                # print('{prefix}LinkHandler: )
                v = len(self.visited)
                nv = len(self.not_visited)
                self.__log('found {} visited links and {} not-visited links'.format(v, nv))
        if len(self.not_visited) == 0 and initial_url is not None:
            self.not_visited = [initial_url]
        self._visit_on_iter = visit_on_iter
        self._save_on_iter = save_on_iter
        self.index = 0
        self._verbose = verbose

    def __log(self, message):
        output = (
            '{prefix}LinkHandler: {message}{suffix}'
        ).format(
            prefix=self._verbose_prefix, 
            suffix=self.__verbose_suffix,
            message=message
        )
        print(output)

    def __to_relative_path(self, full_url):
        url = re.sub(r"http[s]*://www.americanliterature.com", "", full_url)
        return url

    def visit(self, link):
        l = self.__to_relative_path(link)
        if self._verbose:
            self.__log('visiting link {}'.format(l))

        if l in self.not_visited:
            index = self.not_visited.index(l)
            del self.not_visited[index]
            self.visited.append(l)

        if l not in self.visited:
            self.visited.append(l)

    def __sort_not_visited__(self):
        priorities = [
            r"/childrens-stories/.+",
            r"/author/[^\/]+/short-story/.+",
            r"/author/[^\/]+/book/.+",
            r"/author/[^\/]+/fairy-tale/.+"
        ]
        def compareable(l1, l2):
            l1_match_reg_id, l2_match_reg_id = -1, -1
            for i, reg in enumerate(priorities):
                if bool(re.match(reg, l1)) and l1_match_reg_id == -1:
                    l1_match_reg_id = len(priorities) - i
                
                if bool(re.match(reg, l2)) and l2_match_reg_id == -1:
                    l2_match_reg_id = len(priorities) - i

            return l1_match_reg_id, l2_match_reg_id

        def lt(l1, l2):
            c1, c2 = compareable(l1, l2)
            return c1 < c2

        def quicksort(arr, lo, hi, less_than=lambda l1, l2: l1 < l2, ascending=True):
            def swap(arr, i, j):
                helper = arr[i]
                arr[i] = arr[j]
                arr[j] = helper

            def partition(arr, lo, hi):
                pivot = arr[hi]
                i = lo
                for j in range(lo, hi):
                    if less_than(pivot, arr[j]):
                        swap(arr, i, j)
                        i += 1
                    
                swap(arr, i, hi)
                return i

            if lo < hi:
                p = partition(arr, lo, hi)
                quicksort(arr, lo, p - 1)
                quicksort(arr, p + 1, hi)

        quicksort(self.not_visited, 0, len(self.not_visited) - 1, less_than=lt)
    
    def add(self, links):
        if type(links) == list:
            for link in links:
                self.add(link)
            self.__sort_not_visited__()
        elif type(links) == str:
            l = self.__to_relative_path(links)
            if l not in self.visited and l not in self.not_visited:
                if self._verbose:
                    self.__log('adding link {l}'.format(l=l))
                self.not_visited.append(l)
        else: raise TypeError('incorrect type, expected list or str but got {t}'.format(t=str(type(links))))

    def __arr_to_str__(self, arr):
        s = ''
        for elem in arr:
            s += self.__to_relative_path(elem) + '\n'
        return s

    def save(self):
        self.__save__(self.__get_rootpath__())

    def __get_rootpath__(self):
        rootpath = pathlib.Path(__file__).parent.absolute()
        return rootpath

    def __get_filepaths__(self, rootpath):
        visited_path = os.path.join(rootpath, 'visited.txt')
        notvisited_path = os.path.join(rootpath, 'not_visited.txt')
        return visited_path, notvisited_path

    def __load__(self, rootpath):
        def __read__(filepath):
            f = open(filepath, 'r')
            raw = f.read()
            f.close()
            raw = raw.strip()
            parsed = re.split(r"\n+", raw)
            output = [p for p in parsed if p.strip() != '']
            return output
            

        visited_path, notvisited_path = self.__get_filepaths__(rootpath)
        
        self.visited = __read__(visited_path)
        self.not_visited = __read__(notvisited_path)

    def __save__(self, rootpath):
        # f = open('./visited.txt', 'w')
        # f.write()
        visited_path, notvisited_path = self.__get_filepaths__(rootpath)

        v = self.__arr_to_str__(self.visited)
        nv = self.__arr_to_str__(self.not_visited)

        f = open(visited_path, 'w')
        f.write(v)
        f.close()

        f = open(notvisited_path, 'w')
        f.write(nv)
        f.close()


    def __getitem__(self, key):
        if type(key) == int:
            return self.not_visited[key]
        else:
            raise ValueError('index must be integer')
        
    def __len__(self):
        return len(self.not_visited)

    def __iter__(self):
        return LinkIterator(self)

    def __str__(self):
        msg = (
            'Visited: {v}\n'
            'Not visited: {n}'
        ).format(
            v=self.visited,
            n=self.not_visited
        )
        return msg
    

class LinkIterator:
    def __init__(self, linkhandler):
        self.index = 0
        self.linkhandler = linkhandler

    def __next__(self):
        if self.index < len(self.linkhandler.not_visited):
            link = self.linkhandler.not_visited[self.index]
            if self.linkhandler._visit_on_iter:
                self.linkhandler.visit(link)
            else:    
                self.index += 1

            if self.linkhandler._save_on_iter:
                self.linkhandler.save()
            return link
        else:
            raise StopIteration