import re
import os
import pandas as pd

def get_filelist(rootdir='./manually/'):
    files = []
    for dirName, subDirList, fileList in os.walk(rootdir):
        for fname in fileList:
            if fname.split('.')[1].lower() == 'txt':
                files.append(dirName + fname)
    return files

def read_file(filename):
    f = open(filename, 'r')
    content = f.read()
    f.close()
    title, author, source, paragraphs = None, None, None, []
    title_m = re.search(r"(?<=title: )([^\n])+", content)
    author_m = re.search(r"(?<=author: )([^\n])+", content)
    source_m = re.search(r"(?<=source: )([^\n])+", content)

    if title_m is not None:
        title = title_m.group(0)
    if author_m is not None:
        author = author_m.group(0)
    if source_m is not None:
        source = source_m.group(0)
    
    raw_paragraphs = re.split(r"\n{2,}", content)[1:]

    for paragraph in raw_paragraphs:
        if paragraph.strip() == '':
            continue
        label, text, label_explanation = None, None, None
        pre_idx = paragraph.find(';') # find first occurence of ;
        post_idx = paragraph.rfind(';') # find last occurence of ;
        label = paragraph[:pre_idx]
        text = paragraph[pre_idx+1:post_idx]
        label_explanation = paragraph[post_idx+1:]
        
        paragraphs.append({'label': label, 'text': text, 'label_explanation': label_explanation})
    
    return {'title': title, 'author': author, 'source': source, 'paragraphs': paragraphs}

def combine():
    files = get_filelist()
    df_data = {'text': [], 'bias': [], 'bias_explanation': []}
    print(files)
    for file in files:
        parsed = read_file(file)
        ps = parsed['paragraphs']

        for p in ps:
            label, text, label_explanation = p['label'], p['text'], p['label_explanation']
            df_data['bias'].append(label)
            df_data['text'].append(text)
            df_data['bias_explanation'].append(label_explanation)

    df = pd.DataFrame(data=df_data)
    return df

if __name__ == '__main__':
    df = combine()
    df.to_csv('./manually_labeled_textmined.csv', sep=';', index=False)
    
    print(df.head())
    df = pd.read_csv('./manually_labeled_textmined.csv', sep=';')
    print(df.head())




