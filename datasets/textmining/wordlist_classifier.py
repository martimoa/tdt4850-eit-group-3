import pandas as pd
import os
import re
from tqdm import tqdm

def read_text(filepath):
    f = open(filepath, 'r')
    file_content = f.read()
    f.close()

    parts = file_content.split('\n')
    title, author, source, content = None, None, None, None

    title_m = re.search(r"(?<=title: )([^\n])+", parts[0])
    author_m = re.search(r"(?<=author: )([^\n])+", parts[1])
    source_m = re.search(r"(?<=source: )([^\n])+", parts[2])

    if title_m is not None:
        title = title_m.group(0)
    if author_m is not None:
        author = author_m.group(0)
    if source_m is not None:
        source = source_m.group(0)

    content = file_content.split('\n\n', 1)
    content = content[1].strip()
    
    paragraphs = re.split(r"(\n{2,})", content)

    for i in range(len(paragraphs)):
        p = paragraphs[i]
        p = re.sub(r"(\n*)", "", p)
        paragraphs[i] = p

    return title, author, source, paragraphs

def get_filelist(rootdir='./texts/'):
    files = []
    for dirName, subDirList, fileList in os.walk(rootdir):
        for fname in fileList:
            files.append(dirName + fname)
    return files

def bias_count(biased_words_df, gender, paragraph, gendercol='gender', wordcol='word'):
    p = paragraph.strip().lower()
    p = re.sub(r"[.:,\"\'\!\?\n]+", '', p)
    genderset = biased_words_df[(biased_words_df[gendercol] == gender)]
    count = 0
    if p != '':
        # print('This is the paragraph:\n', repr(p), '\n\n')
        words = re.split(r"\s+", p)
        
        for word in words:
            if word in genderset[wordcol].unique():
                count += 1
                
    return count

def get_datarow(title, author, url_source, file_source, paragraph, paragraph_idx, male_count, female_count, neutral_count):
    return [title, author, url_source, file_source, paragraph, paragraph_idx, male_count, female_count, neutral_count]

def biased(male_count, female_count, neutral_count):
    if male_count > female_count and male_count > neutral_count:
        return 'm'
    elif female_count > male_count and female_count > neutral_count:
        return 'f'
    elif neutral_count > female_count and neutral_count > male_count:
        return 'n'
    else:
        return 'n'

def main(args=None):
    df = pd.read_csv('../gendered_words/gendered_words.csv', sep=',')
    genders = ['m', 'f', 'n']

    files = get_filelist()
    
    data = []
    for file in tqdm(files):
        title, author, source, paragraphs = read_text(file)
        male_count, female_count, neutral_count = 0, 0, 0

        for i in range(len(paragraphs)):
            p = paragraphs[i]
            male_count = bias_count(df, 'm', p)
            female_count = bias_count(df, 'f', p)
            neutral_count = bias_count(df, 'n', p)
        
            data.append(get_datarow(title, author, source, file, p, i, male_count, female_count, neutral_count))
    
    columns = [ 'title', 
                'author', 
                'url_source', 
                'file_source', 
                'paragraph', 
                'paragraph_idx', 
                'male_count', 
                'female_count', 
                'neutral_count'
            ]
    dataset = pd.DataFrame(data=data, columns=columns)
    dataset['text'] = dataset['paragraph']
    dataset['bias'] = dataset.apply(lambda x: biased(x['male_count'], x['female_count'], x['neutral_count']), axis=1)
    dataset.to_csv('./dataset.csv', sep=';', index=False)

if __name__ == '__main__': 
    main()


