# /datasets/gendered_words

This folder contains a .json file containing a dataset of English words with the 'natural gender' of several words. 
The source of this file is github user ecmonsen, here:
https://github.com/ecmonsen/gendered_words