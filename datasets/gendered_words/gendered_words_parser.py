import json
import pandas as pd


'''
Parse the gendered_words.json and save it to a .csv file in the current working directory
'''

def parse():
    f = open('./gendered_words.json')
    content = json.load(f)

    words = [item['word'] for item in content]
    # wornet_sensenos = [item['wordnet_senseno'] for item in content]
    genders = [item['gender'] for item in content]


    data = {
        'word': words, 
        # 'wornet_senseno': wordnet_sensenos, 
        'gender': genders
    }

    df = pd.DataFrame(data=data)
    return df

if __name__ == '__main__':
    df = parse()

    print(df.head())
    print(df.shape)

    df.to_csv('./gendered_words.csv')