# Experts in Teamwork - Group 3
me too!!!
This git repository (aka. git 'repo') will contain/contains code contributions and datasets for the group project in Experts in Teamwork for the spring of 2021. 
The team consists of seven members, with differing technical/subject backgrounds. 

## Project
Create a tool for detecting gender bias from text created by children through Digital Storytelling (DST).

## Repository Structure
The repository is divided into two main parts, `/src` and `/datasets`. The `/src` folder should contain only source code, preferably written in python version 3.8.0, but we can change this if required. The `/datasets` folder should contain datasets in the form of either `.csv` files or `.txt` files structured in meaninfull way. It might be a good idea to separate the different datasets into sub-folders inside `/datasets`, if there are many different data sources. For every dataset there shold be a corresponding `DATASET-NAME_README.md` file that describes the structure of the data, if the structure is not obvious.

# How do I start coding in this project?
You will need to install `git` to your computer, to be able to get the code of this repository to your local machine. As a supplement to Marius' crash course on using git, you can also check out [this](https://www.youtube.com/watch?v=SWYqp7iY_Tc&ab_channel=TraversyMedia) 30-ish minute long guide on git.

Using the correct version of Python for the project is the most important thing. We will be using version 3.8, because this version will give us more flexibility in machine learning frameworks to use (tensorflow only supports versions <=3.8, but use Pytorch whenever possible).

## Installation
Follow the appropriate guide for your operating system below.
## Windows
If you already have python installed, check that you have the correct version by running `python -V` in a command prompt. If the version is 3.8.X you're good to go, if not, follow the guide below. 

1. Download the `Windows installer (32-bit)` or `Windows installer 64-bit` installer [here](https://www.python.org/downloads/release/python-387/) (files are listed at the bottom of the page).
2. Once downloaded, run it and follow the on-screen instructions. 
3. Once Python is installed, open a command prompt and type `python help`. You should see output from python, if you see message like 'command python does not exist' or similar, python has not installed correctly. 
4. The next step is to install `pip`, which is the package manager for Python. The package manager allows us to use pre-made libraries and frameworks in our own projects, such as pytorch, tensorflow, numpy, sklearn, etc. To install pip for windows, download the `get-pip.py` python script from [this webpage](https://bootstrap.pypa.io/get-pip.py). E.g. go to the link and press `Ctrl + S` and save the file to your download directory. 
5. Once the file is downloaded open a new command prompt, and type `cd C:\users\YOUR_USERNAME_HERE\downloads` and hit enter.
6. Run the `get-pip.py` script by typing `python get-pip.py` and hit enter. 
7. Once the script completes executing, type the following in the comand prompt and hit enter to verify that pip was installed correctly `pip -V`

## MacOS
Follow [this guide](https://docs.python-guide.org/starting/install3/osx/)

## Linux
If you're using Linux, I expect you already know. 

## Code work outline
- [X] Initialize git repo
- [ ] Gather data
- [ ] Process data
- [ ] Define models
- [ ] Train models
- [ ] ...
- [ ] Profit!
