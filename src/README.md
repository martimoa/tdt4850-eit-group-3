# src
Containing different files used during the project.

## Labler
1. Run 'pip install -r src/labler_reqs.txt'
2. 'python labler.py -i/--ifile \<inputfile> -o/--ofile \<outputfile>'
   1. Expects .csv file
   2. outputfile is optional
3. Output is now ready in datasets folder.
