import torch
import torch.nn as nn
import pandas as pd
import numpy as np
import transformers
from transformers import AutoModel
import dataloader
from transformers import AdamW
import prepocessing
from sklearn.metrics import accuracy_score as accuracy
from sklearn.metrics import precision_score as precision
from sklearn.metrics import f1_score as f1
from sklearn.metrics import recall_score as recall
from sklearn.model_selection import KFold
from sklearn.utils.class_weight import compute_class_weight
from sklearn.model_selection import train_test_split
from joblib import dump, load
import os
import pathlib
parent_dir = pathlib.Path(__file__).parent.absolute()

class ANN(nn.Module):
    def __init__(self, label_defs):
        super(ANN, self).__init__()

        self.input_layer = nn.Linear(768, 512)
        self.dropout1 = nn.Dropout(0.1)
        self.relu1 = nn.ReLU()

        self.hidden_layer = nn.Linear(512, 256)
        self.dropout2 = nn.Dropout(0.1)
        self.relu2 = nn.ReLU()

        self.output_layer = nn.Linear(256, len(label_defs.keys()))
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, embeddings):
        x = self.input_layer(embeddings)
        x = self.relu1(x)
        x = self.dropout1(x)

        x = self.hidden_layer(x)
        x = self.relu2(x)
        x = self.dropout2(x)

        x = self.output_layer(x)
        x = self.softmax(x)
        return x

class BERTANN(nn.Module):
    def __init__(self, bert, label_defs={0: 'not_biased', 1: 'biased'}, ann=None):
        super(BERTANN, self).__init__()

        if label_defs is None:
            raise TypeError('Argument label_defs: expected dict but received None')
        if type(label_defs) != dict:
            err = 'Argument label_defs: expected dict but received {t}'.format(t=type(label_defs))
            raise TypeError(err)

        self.bert = bert
        for param in self.bert.parameters():
            param.requires_grad = False
        
        self.ann = ann if ann is not None else ANN(label_defs)
        self.label_defs = label_defs


    def forward(self, tokens, attention_mask):
        out = self.bert(tokens, attention_mask=attention_mask)
        cls_hs = out.last_hidden_state[:,0,:] # get CLS index/token of input, corresponds to classification token
        x = self.ann(cls_hs)
        return x

    def predict(self, samples, padding=70, return_probabilities=False):
        tokenized = prepocessing.tokenize(samples, padding=padding)
        tokens, masks = torch.tensor(tokenized['input_ids']), torch.tensor(tokenized['attention_mask'])
        x = self.forward(tokens, masks)
        probs = torch.exp(x)
        values, indeces = torch.max(probs, 1) # maximum probability along dimension 1 (e.g. the second dimension)
        
        if return_probabilities:
            return indeces.numpy(), values.detach().numpy()

        return indeces.numpy()
    
    def save(self, filename):
        to_save = {
            'ann': self.ann,
            'label_defs': self.label_defs
        }
        dump(to_save, filename)

    def load(filename):
        loaded = load(filename)
        ann = loaded['ann']
        label_defs = loaded['label_defs']
        bert = AutoModel.from_pretrained('bert-base-uncased')
        return BERTANN(bert, label_defs=label_defs, ann=ann)

if __name__ == '__main__':
    print('BERTANN')
    df = dataloader.load(binary=True)
    samples, labels = df['samples'].to_numpy(), df['labels'].to_numpy()

    num_folds = 5
    batch_size = 42
    epochs = 3
    max_sample_length = 70
    learning_rate = 1e-5

    label_defs = {0: 'not_biased', 1: 'biased'}
    bert = AutoModel.from_pretrained('bert-base-uncased')
    
    tokenized = prepocessing.tokenize(samples, padding=max_sample_length)
    tokens = torch.tensor(tokenized['input_ids'])
    masks = torch.tensor(tokenized['attention_mask'])
    labels = torch.tensor(list(labels))

    dataset = torch.utils.data.TensorDataset(tokens, masks, labels)
    kfold = KFold(n_splits=num_folds, shuffle=True)
    class_weights = torch.tensor(compute_class_weight('balanced', classes=np.unique(labels), y=labels.tolist()), dtype=torch.float)

    BEST_F1 = -np.inf
    BEST_MODEL = None

    for fold, (train_idxs, test_idxs) in enumerate(kfold.split(dataset)):
        print('------------------------FOLD {}------------------------'.format(fold))


        train_sampler = torch.utils.data.SubsetRandomSampler(train_idxs)
        test_sampler = torch.utils.data.SubsetRandomSampler(test_idxs)
        
        train_dataloader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, sampler=train_sampler)
        test_dataloader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, sampler=test_sampler)
        
        model = BERTANN(bert, label_defs)

        optimizer = AdamW(model.parameters(), lr=learning_rate)
        loss_function = nn.NLLLoss(weight=class_weights)
        model.train()

        for epoch in range(epochs):
            print('Epoch {}'.format(epoch))            
            current_loss = 0
            for index, batch in enumerate(train_dataloader):
                print('\t\tBatch {} current loss {:.4f}'.format(index, current_loss / (index + 1)))

                tokens, masks, labels = batch
                optimizer.zero_grad()

                preds = model(tokens, masks)
                loss = loss_function(preds, labels)
                
                loss.backward()

                torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)

                optimizer.step()
                current_loss += loss.item()
            print('Epoch {}: loss {:.4f}'.format(epoch, current_loss / len(train_dataloader)))

        print('------------------------EVAL------------------------')
        model.eval()
        eval_loss = 0
        eval_preds = []
        with torch.no_grad():
            mean_acc = 0
            mean_prec = 0
            mean_f1 = 0
            mean_rec = 0
            for index, batch in enumerate(test_dataloader):
                tokens, masks, labels = batch    
                preds = model(tokens, masks)
                loss = loss_function(preds, labels)
                eval_loss += loss.item()
                probs = torch.exp(preds)
                _, predicted = torch.max(probs, 1)
                predicted = predicted.numpy()
                labels = labels.numpy()
                acc = accuracy(labels, predicted)
                prec = precision(labels, predicted)
                f1_score = f1(labels, predicted)
                rec = recall(labels, predicted)
                mean_acc += acc
                mean_prec += prec
                mean_f1 += f1_score
                mean_rec += rec
                print('\tEval iteration {} Accuracy: {:.4f} Precision: {:.4f} Recall: {:.4f} F1 score: {:.4f}'.format(index, acc, prec, rec, f1_score))
                # eval_preds += np.argmax(probs.detach().numpy(), axis=1).tolist()
            mean_acc = mean_acc / len(test_dataloader)
            mean_prec = mean_prec / len(test_dataloader)
            mean_rec = mean_rec / len(test_dataloader)
            mean_f1 = mean_f1 / len(test_dataloader)
            print('\tScores after evaluation: Accuracy: {:.4f} Precision: {:.4f} Recall: {:.4f} F1 score: {:.4f}'.format(mean_acc, mean_prec, mean_rec, mean_f1))
            if mean_f1 > BEST_F1:
                filename = 'BERTANN.joblib'
                savepath = os.path.join(parent_dir, './parameters/', filename)
                print('\tBest F1 score so far, saving model as {}'.format(savepath))
                BEST_F1 = mean_f1
                BEST_MODEL = model
                model.save(savepath)