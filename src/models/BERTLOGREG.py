from sklearn.linear_model import LogisticRegression
import numpy as np
import prepocessing
import dataloader
from sklearn.utils.class_weight import compute_class_weight
from sklearn.preprocessing import StandardScaler
import torch
import torch.nn as nn
from transformers import AutoModel
from sklearn.metrics import accuracy_score as accuracy
from sklearn.metrics import precision_score as precision
from sklearn.metrics import f1_score as f1
from sklearn.metrics import recall_score as recall
import pathlib
import os
from joblib import dump, load
parent_dir = pathlib.Path(__file__).parent.absolute()

class BERTLOGREG:
    def __init__(self, bert, model=None, scaler=None):
        self.bert = bert # no need to set nograd on bert, because logreg does not use optimizer
        
        for param in self.bert.parameters():
            param.requires_grad = False

        self.model = model if model is not None else LogisticRegression()
        self.scaler = scaler if scaler is not None else StandardScaler()
        # self.model = LogisticRegression()
        # self.scaler = StandardScaler()

    def fit(self, tokens, masks, labels):
        embeddings = self.bert(tokens, masks)
        cls_hs = embeddings.last_hidden_state[:,0,:] # get CLS index/token of input, corresponds to classification token

        features = self.scaler.fit_transform(cls_hs, labels)
        self.model.fit(features, labels)

    def predict(self, samples, padding=70, return_probabilities=False):
        tokenized = prepocessing.tokenize(samples, padding=padding)
        tokens, masks = torch.tensor(tokenized['input_ids']), torch.tensor(tokenized['attention_mask'])

        embeddings = self.bert(tokens, masks)
        cls_hs = embeddings.last_hidden_state[:,0,:] # get CLS index/token of input, corresponds to classification token

        features = self.scaler.transform(cls_hs)
        
        if return_probabilities:
            probs = self.model.predict_proba(features)
            preds = np.argmax(probs, axis=1)
            outprobs = np.max(probs, axis=1)
            print(type(preds), type(outprobs))
            return preds, outprobs
        
        preds = self.model.predict(features)
        return preds

    def save(self, filename):
        # don't save the bert model, as it is quite large, only save the parts we need to (LogReg classification layer and StandardScaler scaler)
        to_save = {
            'model': self.model,
            'scaler': self.scaler
        }
        dump(to_save, filename)

    def load(filename):
        loaded = load(filename)
        model = loaded['model']
        scaler = loaded['scaler']
        bert = AutoModel.from_pretrained('bert-base-uncased')
        return BERTLOGREG(bert, model=model, scaler=scaler)


if __name__ == '__main__':
    print('-------------------------------------------------------------')
    print('Text classification model using BERT and Logistic Regression.')
    print('-------------------------------------------------------------')
    print('\tLoading dataset...')
    df = dataloader.load(binary=True)
    print('\tLoading done!')
    print('\tLoading BERT model...')
    bert = AutoModel.from_pretrained('bert-base-uncased')
    model = BERTLOGREG(bert)
    print('\tModel loaded!')

    samples, labels = df['samples'].tolist(), df['labels'].tolist()

    max_sample_length = 70

    print('\tPreprocessing data into testing and training sets...')
    processed = prepocessing.preprocess(samples, labels, batch_size=1, test_fraction=0.3, max_sample_length=max_sample_length)
    tokens, masks, labels = processed['train']['tokens'], processed['train']['masks'], processed['train']['labels']
    
    print('\tPreprocessing done!')
    print('----------------------------TRAIN----------------------------')
    print('\tTraining/fitting classification layer...')
    model.fit(tokens, masks, labels)
    print('\tTraining done!')
    test_samples, test_labels = processed['test']['samples'], processed['test']['labels']
    print('----------------------------EVAL-----------------------------')
    print('\tPerforming classification on test set...')
    pred = model.predict(test_samples)
    print('\tClassification done!')
    print('\tComputing evaluation metrics...')
    acc = accuracy(test_labels, pred)
    prec = precision(test_labels, pred)
    rec = recall(test_labels, pred)
    f1_score = f1(test_labels, pred)
    print('\tComputation done!')
    print('Logistic Regression model evaluation results')
    print('Accuracy: {:.4f} Precision: {:.4f} Recall: {:.4f} F1 score: {:.4f}'.format(acc, prec, rec, f1_score))
    filename = 'BERTLOGREG.joblib'
    savepath = os.path.join(parent_dir, './parameters/', filename)
    print('Saving model at {}'.format(savepath))
    model.save(savepath)

    m2 = BERTLOGREG.load(savepath)
    print(m2)
    pred = m2.predict(['Some stuff'])
    print(pred)


