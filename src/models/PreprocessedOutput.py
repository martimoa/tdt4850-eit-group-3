import pprint
class PreprocessedOutput:
    def __init__(self, internals):
        self.__internals = internals

    def __getitem__(self, key):
        if type(key) == int:
            raise KeyError('Integer keys are not allowed indeces of PreprocessedOutput')
        elif type(key) == tuple:
            for item in key:
                if type(item) != str:
                    raise KeyError('Tuple key contains non str type item, cannot use as index')
            
            obj = self.__internals
            for item in key:
                obj = obj[item]
            return obj

        elif type(key) == str:
            return self.__internals[key]

        raise KeyError('No usable key type was found')

    def keys(self):
        return self.__internals.keys()

    def __str__(self):
        printable = {}
        
        def rec(obj, to_print, indent=0):
            ind = indent
            for key in obj.keys():
                # print(key, type(obj[key]))
                if type(obj[key]) == dict:
                    to_print[key] = {}
                    obj[key], t, indt = rec(obj[key], to_print[key], indent=indent+1) 
                    indent = ind
                else:
                    to_print[key] = type(obj[key])
            return obj, to_print, ind
        
        o, p, max_indent = rec(self.__internals, {})
        return pprint.pformat(p, indent=max_indent)