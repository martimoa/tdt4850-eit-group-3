# prerequisites
python v. 3.6 or above
pytorch v. 1.3.1 or above
tensorflow v. 2.0


## Loading models
To load the latest/best model using the ANN classifier, use the following snippet. 
```python
from src.models.BERTANN import BERTANN
model = BERTANN.load('./src/models/parameters/BERTANN.joblib')

some_samples = [
    'This text is not biased',
    'Some other text'
]

predictions = model.predict(some_samples)
print(predictions)

# Or return both the predictions (classes) and the certanties/probabilities
predictions, certanties = model.predict(some_samples, return_probabilities=True) # defaults to False
print(predictions)
print(certanties)
```

To load the latest/best model using the Logistic Regression classifier, use the following snippet. 
```python
from src.models.BERTLOGREG import BERTLOGREG
model = BERTLOGREG.load('./src/models/parameters/BERTLOGREG.joblib')

samples = [
    'This text is not biased',
    'Some other text'
]

predictions = model.predict(samples)
print(predictions)
# The Logistic Regression model can also return probabilities/certanties of the classifications
predictions, probabilities = model.predict(samples, return_probabilities=True)

```