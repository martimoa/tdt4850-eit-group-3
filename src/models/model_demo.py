from BERTANN import BERTANN, ANN
from BERTLOGREG import BERTLOGREG
import pathlib
import os
import dataloader
from sklearn.metrics import recall_score as recall
parent_dir = pathlib.Path(__file__).parent.absolute()

ann_path = os.path.join(parent_dir, './parameters/BERTANN.joblib')
logreg_path = os.path.join(parent_dir, './parameters/BERTLOGREG.joblib')

# ann_model = BERTANN.load(ann_path)
logreg_model = BERTLOGREG.load(logreg_path)

samples = [
    'A programmer must always carry his laptop with him.',
    'Senators need their wives to support them throughout their campaign.',
    'Harry Potter is a wizard.',
    'I am tired of reading gender biased stuff.',
    'A programmer must always carry their laptop with them.',
    'The princess with the pink dress was rescued by the hero'
]

# predictions, probabilities = ann_model.predict(samples, return_probabilities=True)
# Alternatively
# ann_preds = ann_model.predict(samples, return_probabilities=False) # default return_probabilities=False
predictions, probabilities = logreg_model.predict(samples, return_probabilities=True)
classes = {0: 'Neutral', 1: 'Gender biased'}
preds = [(classes[predictions[i]], probabilities[i], samples[i]) for i in range(len(predictions))]

for bias, prob, text in preds:
    print('"{}"'.format(text))
    print('Is classified as {} with certanty of {:.4f}'.format(bias, prob))
    print()

