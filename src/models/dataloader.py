import pandas as pd
import re

def read_file(path):
    f = open(path)
    content = f.read()
    f.close()
    return content

def parse_labeled_story(text):
    reg = r'title:\s*(.*)\n+author:\s*(.*)\n+source:\s*(.*)\n+'
    match = re.match(reg, text)
    title, author, source = match.group(1), match.group(2), match.group(3)
    content = re.sub(reg, '', text)

    metadata = {
        'title': title,
        'author': author,
        'source': source
    }

    paragraphs = re.split(r'\n{2,}', content)
    data = parse_paragraphs(paragraphs)
    return data, metadata

def parse_paragraphs(paragraphs):
    data = []
    for p in paragraphs:
        if p == '':
            continue
        parts = p.split(';')
        label, raw_text, explanation = parts[0], parts[1], parts[2]
        data.append(
            {
                'label': label,
                'text': raw_text,
                'explanation': explanation
            }
        )
    return data

def parse_wino(content):
    # print(content)
    content = re.sub(r"[0-9]+\s+", '', content)
    # content = re.sub(r"\[+", '', content)
    # content = re.sub(r"\]+", '', content)
    pronouns = ['him', 'her', 'he', 'she', 'his', 'hers']
    label_defs = {0: 'M', 1: 'F', -1: 'N'}
    content = content.strip()
    content = re.split(r"\n+", content)
    output = []
    for sample in content:
        match = re.findall(r"\[([^\]]+)\]", sample)
        idx = -1
        for m in match:
            if m in pronouns:
                idx = pronouns.index(m) % 2
        if idx == -1:
            print(sample, 'is neutral')
        s = re.sub(r"\[+", '', sample)
        s = re.sub(r"\]+", '', s)
        s = re.sub(r"\s's", r"'s", s)
        output.append({'text': s, 'label': label_defs[idx]})

    return output, None

def occupation_parser(content):
    lines = re.split(r"\n+", content)
    output = []
    for line in lines:
        parts = re.split(r"\t+", line)
        if len(parts) < 4:
            continue
        occupation = parts[0]
        participant = parts[1]
        sentence = parts[3]

        possessive_pronoun = re.search(r"\$POSS_PRONOUN", sentence)
        nominal_pronoun = re.search(r"\$NOM_PRONOUN", sentence)
        nom_pronouns = ['he', 'she', 'they']
        poss_pronouns = ['his', 'hers', 'their']
        reg = None
        pronouns_to_use = []
        if possessive_pronoun:
            pronouns_to_use = poss_pronouns
            reg = r"\$POSS_PRONOUN"
        elif nominal_pronoun:
            pronouns_to_use = nom_pronouns
            reg = r"\$NOM_PRONOUN"
        
        classes = {0: 'M', 1: 'F', 2: 'N'}
        for i in range(len(pronouns_to_use)):
            bias = classes[i]
            pronoun = pronouns_to_use[i]
            sample = re.sub(reg, pronoun, sentence)
            sample = re.sub(r"\$OCCUPATION", occupation, sample)
            sample = re.sub(r"\$PARTICIPANT", participant, sample)
            output.append({'text': sample, 'label': bias})

    return output, None

def parse_questionaire(df):
    pass

def load(out_text_col='samples', out_lable_col='labels', binary=False):
    csv_files = [
        {
            'path': './datasets/datasets_data_2021-03-10 040509.711705.csv',
            'index_col': 0,
            'text_col': 'Sentance',
            'label_col': 'Bias',
            'sep': ',',
            'parser': parse_questionaire
        }
    ]

    txt_files = [
        {
            'path': './datasets/textmining/manually/cinderella-unknown.txt',
            'parser': parse_labeled_story 
        },
        {
            'path': './datasets/rubegni/manually_labeled/christian_the_brave_knight.txt',
            'parser': parse_labeled_story
        },
        {
            'path': './datasets/rubegni/manually_labeled/the_prince_of_the_northern_lands.txt',
            'parser': parse_labeled_story
        },
        {
            'path': './datasets/MulanBiased.txt',
            'parser': parse_labeled_story
        },
        {
            'path': './datasets/winobias dataset/pro_stereotyped_type2.txt.test',
            'parser': parse_wino
        },
        {
            'path': './datasets/occupation dataset/templates.tsv',
            'parser': occupation_parser
        }
    ]


    all_samples, all_labels = [], []
    for txt in txt_files:
        text = read_file(txt['path'])
        data, metadata = txt['parser'](text)
        texts = [sample['text'] for sample in data]
        labels = [sample['label'] for sample in data]
        all_samples += texts
        all_labels += labels

    for csv in csv_files:
        index_col = None
        if 'index_col' in csv:
            index_col = csv['index_col']

        df = pd.read_csv(csv['path'], sep=csv['sep'], index_col=index_col)
        df_samples = df[csv['text_col']].tolist()
        df_labels = df[csv['label_col']].tolist()
        all_samples += df_samples
        all_labels += df_labels
    
    df = pd.DataFrame({out_text_col: all_samples, out_lable_col: all_labels})
    if binary:
        df.loc[df[out_lable_col] == 'F', out_lable_col] = 1
        df.loc[df[out_lable_col] == 'M', out_lable_col] = 1
        df.loc[df[out_lable_col] == 'N', out_lable_col] = 0
    return df

if __name__ == '__main__':
    df = load()
    print(df.head())
    print(df.shape)
    print(df['labels'].unique())

    import matplotlib.pyplot as plt
    import numpy as np
    sample_lengths = [len(sample.split()) for sample in df['samples'].to_numpy()]
    bins = 80
    font_size = 12
    print('Min {} Max {} Mean {} std {}'.format(np.min(sample_lengths), np.max(sample_lengths), np.mean(sample_lengths), np.std(sample_lengths)))
    print([(len(df[(df['labels'] == val)]), val) for val in df['labels'].unique()])

    plt.title('Histogram of number of words in dataset ({} bins)'.format(bins), fontsize=16)
    plt.ylabel('Number of samples', fontsize=font_size)
    plt.xlabel('Number of words', fontsize=font_size)
    plt.hist(sample_lengths, bins=bins)
    plt.show()

    
    raw_values = [(len(df[(df['labels'] == val)]), val) for val in df['labels'].unique()]
    objects = []
    colors = []
    for l, val in raw_values:
        if val == 'M':
            objects.append('Male bias')
            colors.append('blue')
        elif val == 'F':
            objects.append('Female bias')
            colors.append('red')
        else:
            objects.append('Neutral')
            colors.append('gray')
    
    
    y_pos = np.arange(len(objects))
    values = [l for l, val in raw_values]
    plt.title('Distribution of classes', fontsize=16)
    plt.bar(y_pos, values, align='center', color=colors)
    plt.xticks(y_pos, objects, fontsize=12)
    plt.ylabel('Number of samples', fontsize=12)
    plt.show()