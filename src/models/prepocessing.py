import torch
import torch.nn as nn
import pandas as pd
import numpy as np
from PreprocessedOutput import PreprocessedOutput
from transformers import AutoModel, BertTokenizerFast
from sklearn.model_selection import train_test_split
from torch.utils.data import TensorDataset, DataLoader, RandomSampler, SequentialSampler
from sklearn.utils.class_weight import compute_class_weight
import dataloader

def tokenize(samples, bert_model_str='bert-base-uncased', padding=30):
    tokenizer = BertTokenizerFast.from_pretrained(bert_model_str)
    lens = [len(seq) for seq in samples]
    
    padding_length = padding
    if padding is None:
        padding_length = int(np.mean(lens)) 
    
    tokens = tokenizer.batch_encode_plus(
        list(samples),
        padding='max_length',
        max_length=padding_length,
        truncation=True # if sample has length greater than maximum allowed for model, use only sample[:max_len] of sample (e.g. truncate)
    )
    return tokens

def to_tensors(tuple_lists):
    out = []
    for tup in tuple_lists:
        out.append(torch.tensor(tup))
    return tuple(out)


def preprocess(samples, labels, batch_size=42, random_state=None, test_fraction=0.3, max_sample_length=30):
    tokenized_samples = tokenize(samples, padding=max_sample_length)
    tokens, masks = tokenized_samples['input_ids'], tokenized_samples['attention_mask']
    
    train_tokens, test_tokens, train_masks, test_masks, train_labels, test_labels, train_samples, test_samples = train_test_split(
        tokens, masks, labels, samples,
        random_state=random_state, test_size=test_fraction, stratify=labels
    )
    train_test = train_tokens, test_tokens, train_masks, test_masks, train_labels, test_labels
    train_tokens, test_tokens, train_masks, test_masks, train_labels, test_labels = to_tensors(train_test)

    train_data = TensorDataset(train_tokens, train_masks, train_labels)
    test_data = TensorDataset(test_tokens, test_masks, test_labels)

    train_sampler = RandomSampler(train_data)
    test_sampler = SequentialSampler(test_data)

    train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=batch_size)
    test_dataloader = DataLoader(test_data, sampler=test_sampler, batch_size=batch_size)

    train_class_weights = torch.tensor(compute_class_weight('balanced', classes=np.unique(train_labels), y=train_labels.tolist()), dtype=torch.float)
    test_class_weights = torch.tensor(compute_class_weight('balanced', classes=np.unique(test_labels), y=test_labels.tolist()), dtype=torch.float)

    output = {
        'train': {
            'tokens': train_tokens,
            'masks': train_masks,
            'labels': train_labels,
            'samples': train_samples,
            'dataloader': train_dataloader,
            'class_weights': train_class_weights,
            '_sampler': train_sampler,
            '_tensordataset': train_data,
        },
        'test': {
            'tokens': test_tokens,
            'masks': test_masks,
            'labels': test_labels,
            'samples': test_samples,
            'dataloader': test_dataloader,
            'class_weights': test_class_weights,
            '_sampler': test_sampler,
            '_tensordataset': test_data
        }
    }

    p = PreprocessedOutput(output)

    return p

def preprocess_v2(input_samples, input_labels, batch_size=42, random_state=None, max_sample_length=30, sampler=None):
    samples, labels = list(input_samples), list(input_labels)

    tokenized_samples = tokenize(samples, padding=max_sample_length)
    tokens, masks = tokenized_samples['input_ids'], tokenized_samples['attention_mask']

    # tensor_tokens, tensor_masks, tensor_labels = torch.tensor(tokens), torch.tensor(masks), torch.tensor(labels)
    tensor_tokens = torch.tensor(tokens)
    tensor_masks = torch.tensor(masks)
    tensor_labels = torch.tensor(labels)

    tensor_dataset = TensorDataset(tensor_tokens, tensor_masks, tensor_labels)

    data_sampler = sampler(tensor_dataset) if sampler is not None else RandomSampler(tensor_dataset)

    dataloader = DataLoader(tensor_dataset, sampler=data_sampler, batch_size=batch_size)
    class_weights = torch.tensor(compute_class_weight('balanced', classes=np.unique(labels), y=labels), dtype=torch.float)

    return {
        'tokens': tokens,
        'masks': masks,
        'labels': labels,
        'samples': samples,
        'dataloader': dataloader,
        'class_weights': class_weights,
        '_tensordataset': tensor_dataset,
        '_sampler': data_sampler
    }


class RandomModel:
    def __init__(self, output_len):
        self.output_len = output_len

    def __call__(self, samples):
        return np.random.rand(len(samples), self.output_len)

    def fit(self, X, y):
        return
if __name__ == '__main__':
    # df = dataloader.load()
    
        
    # df.loc[df['labels'] == 'F', 'labels'] = 1
    # df.loc[df['labels'] == 'M', 'labels'] = 1
    # df.loc[df['labels'] == 'N', 'labels'] = 0
        
    # samples, labels = df['samples'], df['labels']
    # processed = preprocess_v2(samples, labels)
    # print(processed.keys())

    X, y = np.random.rand(10, 2), np.random.rand(10, 1)
    print(X, y)

    from sklearn.model_selection import cross_val_score
    model = RandomModel(1)

    print(cross_val_score(model, X, y, cv=3))

    