"""
Small script to convert text files of sentences to
a JSON dataset.
"""
import sys, getopt, pandas, datetime
import numpy as np

def main(argv):
    try:
        opts, _ = getopt.getopt(argv, "i:o:", ["--ifile=", "--ofile"])
    except getopt.GetoptError:
        print('python /', __file__, ' -i <inputfile> -o <outputfile>')
        sys.exit(2)

    input_path = ""
    output_path = "datasets/data_{}.csv".format(datetime.datetime.today())
    for opt, arg in opts:
        if opt in ("-i", "--ifile"):
            input_path = arg
        elif opt in ("-o", "--ofile"):
            output_path = "datasets/" + arg
    try:
        df = pandas.read_csv(input_path)
    except FileNotFoundError as err:
        print('python ', __file__, ' -i <inputfile> -o <outputfile>')
        print(err)
        sys.exit(2)

    df["Bias"] = df["Bias"].str.rstrip()  # Prone to trailing whitespace

    for x in df.index:
        if df.loc[x, "Bias"] not in ("M", "F", "N"):
            df.loc[x, "Bias"] = np.nan

    df.dropna(inplace=True)  # Remove rows with empty cells
    df.to_csv(output_path)

if __name__ == "__main__":
    # Expects commandline:
    # python labler -i <inputfile> -o <outputfile>
    main(sys.argv[1:])
