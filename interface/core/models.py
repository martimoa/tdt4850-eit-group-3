from django.db import models
from model_utils import Choices


class StoryTheme(models.Model):
    title = models.CharField(max_length=150)
    image = models.ImageField(upload_to="themes/")

    def __str__(self):
        return self.title
    

class Submission(models.Model):
    STATUS = Choices("created", "fulfilled", "cancelled")
    created = models.DateTimeField(auto_now_add=True)
    status = models.CharField(choices=STATUS, default=STATUS.created, max_length=20)
    theme = models.ForeignKey(StoryTheme, on_delete=models.DO_NOTHING, blank=True, null=True)
    
    def __str__(self):
        return "[{}] Submission({})".format(self.status, self.pk)
    
class Result(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    sentence = models.TextField()
    prediction = models.CharField(max_length=50)
    probability = models.FloatField()
    submission = models.ForeignKey(Submission, related_name='results', default="", on_delete=models.CASCADE)


    def __str__(self):
        n = 20
        if len(self.sentence) > n:
            return "{}... - {}".format(self.sentence[0:n], self.prediction)
        else:
            return "{} - {}".format(self.sentence, self.prediction)



class SubmissionHeader(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    question = models.CharField(max_length=100)
    answer = models.CharField(max_length=100)
    submission = models.ForeignKey(Submission, related_name='headers', on_delete=models.CASCADE)
