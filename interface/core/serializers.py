from rest_framework import serializers
from .models import Result, SubmissionHeader, Submission

class ResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = Result
        fields = ('sentence', 'prediction', 'probability', 'submission')
        read_only_fields = ('prediction', 'probability', 'submission')
    


class HeaderSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubmissionHeader
        fields = ('question', 'answer', 'submission')
        extra_kwargs = {
            'submission': {'write_only': True}
        }


class SubmissionSerializer(serializers.ModelSerializer):
    headers = HeaderSerializer(many=True)
    class Meta:
        model = Submission
        fields = ('result', 'status', 'headers')
        read_only_fields= ('result', 'status', 'headers')