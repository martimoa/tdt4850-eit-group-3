# Insert trained model parent directory into Python path (to be able to import models)
import sys
import pathlib
import os
project_dir = pathlib.Path(__file__).parent.parent.parent.parent.absolute()
model_dir = os.path.join(project_dir, './src/models/')
sys.path.insert(1, model_dir)

# import model
from BERTLOGREG import BERTLOGREG
model = BERTLOGREG.load(os.path.join(model_dir, './parameters/BERTLOGREG.joblib'))

def predict(sentence, neutral_threshold=0.5):
    samples = [sentence]
    predictions, probabilities = logreg_model.predict(samples, return_probabilities=True)
    classes = {0: 'Neutral', 1: 'Gender biased'}
    preds = [(classes[predictions[i]], probabilities[i], samples[i]) for i in range(len(predictions))]

    bias, prob, sample = preds[0]
    return bias, prob
    