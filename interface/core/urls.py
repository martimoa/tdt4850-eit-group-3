"""core URL Configuration"""
from django.urls import path, include
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from .views import StoryTelling, StoryThemeView, Index

urlpatterns = [
    path('', Index.as_view(), name='index'),
    path('theme/', StoryThemeView.as_view(), name='theme'),
    path('<str:story>/storytelling', StoryTelling.as_view(), name='storytelling'),
    path('api/api-auth/', include('rest_framework.urls')),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)