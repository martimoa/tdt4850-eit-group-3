from django.contrib import admin
from .models import Result, SubmissionHeader, Submission, StoryTheme

class ResultInline(admin.TabularInline):
    model = Result
    readonly_fields =  ['sentence', 'prediction', 'probability']
    extra = 0

class SubmissionAdmin(admin.ModelAdmin):
    inlines = [
        ResultInline,
    ]

# Register your models here.
admin.site.register(Result)
admin.site.register(Submission, SubmissionAdmin)
admin.site.register(SubmissionHeader)
admin.site.register(StoryTheme)