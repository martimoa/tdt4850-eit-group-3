from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import View
from django.http import HttpResponse
from django.contrib import messages
from .models import Result, Submission, SubmissionHeader, StoryTheme
from .serializers import ResultSerializer, SubmissionSerializer, HeaderSerializer
from .src.dummy_nlp import predict

from rest_framework import generics


class Index(View):
    template_name='index.html'
    success_url='theme'
    
    def get(self, request, *args, **kwargs):
        if 'submissionid' in request.session.keys():
            try:
                submission = Submission.objects.get(pk=request.session['submissionid'])
                if submission.status == Submission.STATUS.created:
                    submission.status = Submission.STATUS.cancelled
                submission.save()
            except Submission.DoesNotExist:
                pass
            del request.session['submissionid']
            
        return render(request, self.template_name, {})
    
    def post(self, request, *args, **kwargs):
        submission = Submission.objects.create()
        request.session['submissionid'] = submission.pk
        return redirect(reverse(self.success_url))


class StoryTelling(View):
    template_name='story-telling.html'
    fallback_url='index'

    def get(self, request, *args, **kwargs):
        story_name = kwargs['story']
        try:
            submission = Submission.objects.get(pk=request.session['submissionid'])
            storytheme = StoryTheme.objects.get(title=story_name)
            submission.theme = storytheme
            submission.save()
        except (Submission.DoesNotExist, StoryTheme.DoesNotExist, KeyError):
            messages.error(request, "Wupsi, something went wrong. Please try again.")
            return redirect(reverse(self.fallback_url))
        return render(request, self.template_name, {})
    
    def post(self, request, *args, **kwargs):
        if request.POST:
            sentence = request.POST.get('sentence')
            prediction, probability = predict(sentence)
            prob_percentage = format(probability, '.2%')
            messages.info(request, '{}, with {} certainty'.format(prediction, prob_percentage))

            # Create result
            submission = Submission.objects.get(pk=request.session['submissionid'])

            result = Result.objects.create(
                sentence = sentence,
                prediction = prediction,
                probability = probability,
                submission = submission
            )
            submission.status = Submission.STATUS.fulfilled
            submission.save()

        return redirect(reverse('storytelling', kwargs={'story': kwargs['story']}))


class StoryThemeView(View):
    template_name='story-theme.html'
    fallback_url='index'

    def get(self, request, *args, **kwargs):
        try:
            Submission.objects.get(pk=request.session['submissionid'], status=Submission.STATUS.created)
        except (KeyError, Submission.DoesNotExist):
            messages.error(request, "Wupsi, something went wrong. Please try again.")
            return redirect(reverse(self.fallback_url))

        context = {
            'themes': StoryTheme.objects.all()
        }
        return render(request, self.template_name, context)
class Predict(generics.ListCreateAPIView):
    """@Deprecated"""
    queryset = Result.objects.all()
    serializer_class = ResultSerializer

    def perform_create(self, serializer):
        data = {}
        sentence = self.request.POST.get('sentence')
        data['sentence'] = sentence
        prediction, probability = predict(sentence)
        data['prediction'] = prediction
        data['probability'] = probability

        serializer.save(**data)

class SubmissionView(generics.ListCreateAPIView):
    """@Deprecated"""
    queryset = Submission.objects.all()
    serializer_class = SubmissionSerializer
