# Interface
An interface to display our solution.
## Setup
- Create an working environment
- Run `pip install -r requirements.txt`
- Run `python manage.py migrate`
- Run `python manage.py createsuperuser`
  - Press enter for default values

## Run server :rocket:
- Requires that setup has been done
- Enter working environment
- Run `python manage.py runserver`
- Whola :tada:  Website is ready at localhost:8000
